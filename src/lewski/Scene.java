package lewski;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import org.joml.Vector3f;

public class Scene {

	public ArrayList<MeshObject> objects = new ArrayList<MeshObject>();
	
	public Camera mainCamera;
	
	private Vector3f rotation;
	
	public Light tempLight;
	public Vector3f directionalLight = new Vector3f(-0.2f, 0.5f,-0.1f).normalize();
	
	private float newFov;
	private Vector3f p;
	public Vector3f dir;
	private Vector3f up;
	
	private int[] skybox;
	private int skyboxImgWidth;
	private int skyboxImgHeight;

	public Scene() {
		
		mainCamera = new Camera();
		tempLight = new Light(new Vector3f(0,0,-10), 1);
		//objects.add(new MeshObject());

		CubeObject c1 = new CubeObject(new Vector3f(0,0,0));
		c1.loadColorMap("res/reflectmap1.png");
		c1.loadReflectionMap("res/reflectmap1.png");
		//c1.luminance = 0.5f;
		c1.reflection = 0.5f;
		
		CubeObject c2 = new CubeObject(new Vector3f(0,.2f,2));
		c2.reflection = 0.1f;
		c2.luminance = 1.0f;
		c2.objectColor = Color.white;

		
		CubeObject c3 = new CubeObject(new Vector3f(-2,0,1));
		c3.reflection = 0.1f;
		c3.luminance = 0.1f;
		c3.transparency = 0.8f;
		c3.objectColor = Color.cyan;

		
		MeshObject floor = ModelLoader.loadModel("res/floor1.obj");
		floor.scale(10);
		floor.setPosition(new Vector3f(0,-0.5f,0));
		floor.luminance = 0.2f;
		floor.objectColor = Color.white;
		
		
		objects.add(c1);
		objects.add(c2);
		objects.add(c3);
		objects.add(floor);
		
		newFov = mainCamera.getFOV();
		p = new Vector3f(mainCamera.getPosition());
		dir = new Vector3f(mainCamera.getDirection());
		rotation = new Vector3f(0);//new Vector3f(mainCamera.getDirection());
		up = new Vector3f(mainCamera.getUp());
		
		
		skybox = loadTexture("res/sky2.png");
	}


	private float moveSpeed = 0.03f;
	private float rotateAngle = (float) (Math.PI / 130.0f);
	
	private boolean lookingAround = false;
	private boolean focus = false;
	private Vector3f focusTarget = new Vector3f(0,0,5);
	
	public static boolean moving = false;
	public void update() {
		moving = false;
		Vector3f rot = null;
			
		if(KeyboardInput.focusToggleButton) {
			focus = !focus;
			
		}
		if(KeyboardInput.useMouseToggleButton && !focus) {
			lookingAround = !lookingAround;
		}
		if(KeyboardInput.shadowToggleButton) {
			RayLightCalculator.shadow = !RayLightCalculator.shadow;
		}
		if(KeyboardInput.backFaceCullingToggleButton) {
			RayLightCalculator.backFaceCulling = !RayLightCalculator.backFaceCulling;
		}
		
		//zoom in/out
		if(MouseInput.mouseScroll != 0) {
			newFov += MouseInput.getMouseScroll() * 3.0f;
		}
		mainCamera.setFOV( Util.lerp(mainCamera.getFOV(), newFov, 0.05f) );

		
		
		if(lookingAround) {
			moving = true;
		}
		
		
		//rotate the scene
		if(lookingAround) {
			
			rot = new Vector3f(0);
			
			if(MouseInput.deltaX != 0) {
				float angle = rotateAngle * (MouseInput.deltaX / 5.0f);
				//rot.y = -angle;
				dir = Util.rotateAroundAxis(dir, mainCamera.getUp(), -angle);
			}
			
			if(MouseInput.deltaY != 0) {
				float angle = rotateAngle * (MouseInput.deltaY / 5.0f);
				dir = Util.rotateAroundAxis(dir, Util.cross(mainCamera.getDirection(), mainCamera.getUp()), -angle);
				//rot.x = -angle;
			}
			//rot.x = 0;
			//rot.z = 0;
			rotation = Util.sub(rotation, rot);
			//System.out.println(rot);
			//mainCamera.setDirection(Util.rotatePoint(mainCamera.getDirection(), rot);
			//rotateScene(rot);
			//dir = Util.rotatePoint(dir, rot);
			
		}
		
		
		
		
		
		
		
		//translate the camera
		if(KeyboardInput.left) {
			p.add(Util.mult(Util.cross(up, dir), moveSpeed));
			moving = true;
		}
		if(KeyboardInput.right) {
			p.sub(Util.mult(Util.cross(up, dir), moveSpeed));
			moving = true;
		}
		if(KeyboardInput.fwd) {
			//p.z += moveSpeed;
			p.add(Util.mult(dir, moveSpeed));
			moving = true;
		}
		if(KeyboardInput.back) {
			//p.z -= moveSpeed;
			p.sub(Util.mult(dir, moveSpeed));
			moving = true;
		}
		if(KeyboardInput.down) {
			p.y -= moveSpeed;
			moving = true;
		}
		if(KeyboardInput.up) {
			p.y += moveSpeed;
			moving = true;
		}
	
		
		
		
		//apply the translation
		mainCamera.setPosition( Util.lerp(mainCamera.getPosition(), p, 0.1f));

		if(focus) {
			//mainCamera.setDirection(Util.lerp(mainCamera.getDirection(), Util.sub(focusTarget, mainCamera.getPosition()), 1f));
		}else {
			mainCamera.setDirection(dir);
			//mainCamera.setDirection(Util.lerp(mainCamera.getDirection(), dir, 0.1f));
		}
				
		
		for(MeshObject mo : objects) {
			mo.update();
		}
		
		
	}
	
	public void rotateScene(Vector3f rotation) {
		for(MeshObject mo : objects) {
			if(rotation != null) {
				mo.rotateAround(mainCamera.getPosition(), rotation);
			}
		}
		
		directionalLight = Util.rotatePoint(directionalLight, rotation).normalize();
	}
		
	private int[] loadTexture(String file) {
		int[] tempPixels = null;
		
		try {
			BufferedImage image = ImageIO.read(new File(file));
			skyboxImgWidth = image.getWidth();
			skyboxImgHeight = image.getHeight();
			
			tempPixels = new int[skyboxImgWidth * skyboxImgHeight];
			
			image.getRGB(0, 0, skyboxImgWidth, skyboxImgHeight, tempPixels, 0, skyboxImgWidth);
			
		}catch (IOException e) {
			e.printStackTrace();
		}
		return tempPixels;
	}
	
	float min = 999;
	float max = -999;
	
	public Color getSkyboxColor(Vector3f dir) {
		
		//dir = new Vector3f(dir.normalize());//Util.rotatePoint(dir, rotation));//.normalize());
		
		int yPixel = (int) Util.lerp(skyboxImgHeight, 0, (dir.y + 1.0f) / 2.0f);
		//int xPixel = (int) Util.lerp(0, skyboxImgWidth , (float) (((dir.x + dir.z) + Math.sqrt(2.0)) / (Math.sqrt(2.0)*2.0f)));
		
		float d = Util.dot(new Vector3f(dir.x, 0, dir.z).normalize(), new Vector3f(0,0,-1));
		int xPixel = 0;
		
		if(dir.x < 0) {
			xPixel = (int) Util.lerp(skyboxImgWidth/2, 0, (d + 1.0f) / 2.0f);
		}else {
			xPixel = (int) Util.lerp(skyboxImgWidth/2, skyboxImgWidth, (d + 1.0f) / 2.0f);
		}
		
		return new Color(skybox[xPixel + yPixel * skyboxImgWidth]);
		
	}
	
}
