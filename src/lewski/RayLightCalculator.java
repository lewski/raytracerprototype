package lewski;

import java.awt.Color;
import java.util.Random;

import org.joml.Matrix3f;
import org.joml.Vector3f;

public class RayLightCalculator {

	private static Scene currentScene;
	
	private static int maxBounces = 4;
	private static Random rand;
	
	private static Vector3f pointLight;
	
	private static float t,u,v;
	private static Vector3f N;
	
	public static boolean shadow = false;
	public static boolean backFaceCulling = true;
	
	public static Color globalAmbiantColor = new Color(10,10,7);
	
	public static void init(Scene s) {
		currentScene = s;
		rand = new Random(5868);
		
		pointLight = new Vector3f(0.5f, 1.0f, -1f);
	}
	
	public static Color getLight(Ray ray, float energy, Color spectrum, int bounce) {
				
		Color finalColor = globalAmbiantColor;
		
		if(bounce > maxBounces) {
			return Util.spectrumMult(finalColor, spectrum);
		}
		
		float closestHit = Float.MAX_VALUE;

		/*
		if(Util.dot(ray.getDirection(), new Vector3f(1,0,0)) > 0.9999f) {
			return Color.red;
		}
		
		if(Util.dot(ray.getDirection(), new Vector3f(0,1,0)) > 0.9999f) {
			return Color.green;
		}
		
		if(Util.dot(ray.getDirection(), new Vector3f(0,0,1)) > 0.9999f) {
			return Color.blue;
		}
		*/
		//float fuzz = 0.0003f;
		//Vector3f randoRayDir = new Vector3f(ray.getDirection().x + (rand.nextFloat()-0.5f) * fuzz, ray.getDirection().y + (rand.nextFloat()-0.5f) * fuzz, ray.getDirection().z + (rand.nextFloat()-0.5f) * fuzz);
		//Ray randoRay = new Ray(ray.getOrigin(), randoRayDir);
		
		//ray = randoRay;
		
		for(MeshObject mo : currentScene.objects) {
			
			float luminance = mo.luminance;
			float reflection = mo.reflection;
			float transparency = mo.transparency;
			float roughness = mo.roughness;
			
			Color objectColor = mo.objectColor;
			
			for(Matrix3f f : mo.getFaces()) {
				Vector3f v0 = Util.add(mo.getVertexPosition().get((int) f.m00), mo.getPosition());
				Vector3f v1 = Util.add(mo.getVertexPosition().get((int) f.m10), mo.getPosition());
				Vector3f v2 = Util.add(mo.getVertexPosition().get((int) f.m20), mo.getPosition());
	
				if(rayTriangleIntersect(ray.getOrigin(), ray.getDirection(), v0, v1, v2) && t < 40) {
				
					Color ambiantColor = globalAmbiantColor;
					
					if(t < closestHit) {
						closestHit = t;
					}else {
						break;
					}
					
					//dont lose energy here
					if(mo.hasColorMap) {
						//ambiantColor = mo.getColorMapValue(u, v);
						spectrum = Util.add(spectrum, mo.getColorMapValue(u, v));
					}
					if(mo.hasReflectionMap) {
						reflection = mo.getReflectionMapValue(u,v);
					}
					//calculate world hit position using Barycentric Coordinates
					Vector3f hitPoint = new Vector3f(Util.mult(v0, (1 - u - v)).add(Util.mult(v1, u)).add(Util.mult(v2, v)));
					
					//Vector3f hitPoint = v0;
					//hitPoint = Util.add(hitPoint, Util.mult(Util.sub(v1, v0), (1 - u - v)));
					//hitPoint = Util.add(hitPoint, Util.mult(Util.sub(v2, v0), v));
					
					/*
					int R = (int) (u * 255);
					int G = (int) (v * 255);
					int B = (int) ((1 - u - v) * 255);
					*/
					
					//luminecensesdfds
					if(luminance > 0) {
						finalColor = Util.add(finalColor, Util.mult(objectColor, luminance));
						spectrum = Util.add(spectrum, objectColor);
					}
					
					Vector3f reflectionRayDir = Util.sub(ray.getDirection(),(Util.mult(N, Util.dot(ray.getDirection(), N) * 2.0f))).normalize();
					//reflection
					if(reflection > 0 && energy > 0) {
						Ray reflectRay = new Ray(hitPoint, reflectionRayDir);
						finalColor = Util.add(finalColor, Util.mult(getLight(reflectRay,reflection * energy, spectrum, bounce + 1), reflection));
					}
					
					/*
					if(transparency > 0 && energy > 0) {
						Vector3f refractedDir = Util.refractedRay(ray.getDirection(), N, 1.0f, mo.indexOfRefraction);
						Ray refractedRay = new Ray(hitPoint,refractedDir);
						finalColor = Util.add(finalColor, Util.mult(getLight(refractedRay, transparency * energy, spectrum, bounce + 1), transparency));
					}
					 */
					
					

					//if there is any energy left over, its our diffuse ray
					/*
					if(energy > 0) {
						float fuzz = 0.01f;
						Vector3f diffuseDir = new Vector3f(reflectionRayDir.x + (rand.nextFloat()-0.5f) * fuzz, reflectionRayDir.y + (rand.nextFloat()-0.5f) * fuzz, reflectionRayDir.z + (rand.nextFloat()-0.5f) * fuzz).normalize();
						Ray diffuseRay = new Ray(hitPoint, diffuseDir);
						finalColor = Util.add(finalColor, Util.mult(getLight(diffuseRay, energy, spectrum, bounce + 1), energy));
					}
					*/
					

					

					
					//skybox
					//finalColor = Util.average(finalColor, Util.mult(currentScene.getSkyboxColor(reflectionRay), 0.3f));
					
					//simple fog
					float dist = Util.distance(hitPoint, currentScene.mainCamera.getPosition());
					if(dist > 0.0f){
						finalColor = Util.lerp(finalColor, globalAmbiantColor, dist / 60.0f);	
					}
					
					
					//test point
					/*
					int index = 0;
					for(float x = -5; x <= 5; x += 0.5f) {
						for(float z = -5; z <= 5; z += 0.5f) {
							index++;
							
							Color c = new Color(255,0,255);
							
							if(x == -5) {
								c = Color.cyan;
							}
							if(z == -5) {
								c = Color.green;
							}
							
							if(Util.distance(hitPoint, new Vector3f(x,-0.5f,z)) < 0.1f) {
								finalColor = Util.average(finalColor,c);
							}
						}
					}
					*/
					
					//simple shadow
					/*
					if(shadow) {
						for(Matrix3f f2 : mo.getFaces()) {
							v0 = Util.add(mo.getVertexPosition().get((int) f2.m00), mo.getPosition());
							v1 = Util.add(mo.getVertexPosition().get((int) f2.m10), mo.getPosition());
							v2 = Util.add(mo.getVertexPosition().get((int) f2.m20), mo.getPosition());
							
							Vector3f shadowSamplePoint = Util.add(hitPoint, Util.mult(N, -0.001f));//raise the sample a little of the surface so it can hit itself 
							Ray shadowRay = new Ray(shadowSamplePoint, Util.sub(shadowSamplePoint, new Vector3f(-1,-1,-10)));
							
							if(rayTriangleIntersect(shadowRay.getOrigin(), shadowRay.getDirection(), v0, v1, v2)) {
								finalColor = Util.average(finalColor, Color.black);
							}
						}
					}
					*/
	
					
	
					
					/*
					if(bounce < maxBounces) {
						Ray bounceRay = new Ray(hitPoint, reflectionRay);
						Util.average(finalColor, getLight(bounceRay, finalColor, bounce + 1));
					}
					*/
					
					
					
				}else {
					//finalColor = currentScene.getSkyboxColor(ray.getDirection());
				}
				
				
				if(energy > 0) {
					float dot = Util.dot(ray.getDirection(), currentScene.directionalLight);
					
					if(dot >= 0.5 && dot <= 1) {
					//	finalColor = Util.lerp(ambiantColor, Color.white, (float)dot);
					}else {
					//	finalColor = ambiantColor;
					}
				}
			}
		}

		//if(bounce >= maxBounces) {
			return Util.spectrumMult(finalColor, spectrum);
			//return finalColor;
			//System.out.println("x: " + ray.getDirection().x + "\ty: " + ray.getDirection().y + "\tz: " + ray.getDirection().z);
			//return currentScene.getSkyboxColor(ray.getDirection());
		//}else {
			//return Util.average(finalColor, getLight(ray, finalColor, bounce + 1));
		//}
		
		
	}
	
	private static boolean rayTriangleIntersect( Vector3f orig, Vector3f dir, Vector3f v0, Vector3f v1, Vector3f v2) {
		
		float kEp = 0.00000001f;
		
		//Moller-trumbore algorithm
		Vector3f v0v1 = Util.sub(v1, v0);
		Vector3f v0v2 = Util.sub(v2, v0);
		Vector3f pVec = Util.cross(dir, v0v2);
		N = Util.cross(v0v1, v0v2);//.normalize();
		float det = Util.dot(v0v1, pVec);
		
		if(backFaceCulling) {
			//check if the determinant is negative (close too) and cull the face if so
			if(det < kEp) {
				return false;
			}
		}else {
			//check if triangle is parallel to direction so we don't draw it
			if(Math.abs(det) < kEp) {
				return false;
			}
		}
		
		float invDet = 1.0f / det;
		
		Vector3f tVec = Util.sub(orig, v0);
		u = Util.dot(tVec, pVec) * invDet;
		if ( u < 0 || u > 1 ) {
			//if our triangle u coordinate is outside of the triangle, we missed it
			return false;
		}
		
		Vector3f qVec = Util.cross(tVec, v0v1);
		v = Util.dot(dir, qVec) * invDet;
		if( v < 0 || u + v > 1 ) {
			return false;
		}
		
		t = Util.dot(v0v2, qVec) * invDet;
		
		if( t < 0 ) { //if the distance is negative, the object is behind the camera
			return false;
		}
		
		return true;
	}
	
	private static boolean rayTriangleIntersectSlow( Vector3f orig, Vector3f dir, Vector3f v0, Vector3f v1, Vector3f v2) {
		
		float kEp = 0.00000001f;
		
		Vector3f v0v1 = Util.sub(v1, v0);
		Vector3f v0v2 = Util.sub(v2, v0);
		N = Util.cross(v0v1, v0v2);
		float denom = Util.dot(N, N);
		
		float NdotRayDirection = Util.dot(N, dir);
		if(Math.abs(NdotRayDirection) < kEp) {
			return false; //parallel, no intersection
		}
		
		float d = Util.dot(N, v0);
		
		t = (Util.dot(N, orig) + d) / NdotRayDirection;
		if(t < 0) return false;
		
		Vector3f P = Util.add(orig, Util.mult(dir, t));
		
		Vector3f C;
		
		Vector3f edge0 = Util.sub(v1, v0);
		Vector3f vp0 = Util.sub(P, v0);
		C = Util.cross(edge0, vp0);
		if(Util.dot(N, C) < 0) return false;
		
		Vector3f edge1 = Util.sub(v2, v1);
		Vector3f vp1 = Util.sub(P, v1);
		C = Util.cross(edge1, vp1);
		if((u = Util.dot(N, C)) < 0) return false;
		
		Vector3f edge2 = Util.sub(v0, v2);
		Vector3f vp2 = Util.sub(P, v2);
		C = Util.cross(edge2, vp2);
		if((v = Util.dot(N, C)) < 0) return false;
		
		u /= denom;
		v /= denom;
		
		return true;
		
	}
	
	
	public static Scene getScene() { return currentScene; }
	public static Vector3f getViewPoint() { return currentScene.mainCamera.getPosition(); }
	public static Vector3f getDirection() { return currentScene.mainCamera.getDirection(); }
	
	public static void setScene(Scene s) {
		currentScene = s;
	}
}
