package lewski;

import java.awt.Color;

import org.joml.Matrix4x3fc;
import org.joml.Vector3f;

public class Util {

	public static float lerp(float val1, float val2, float percent) {
		return val1 + percent * (val2 - val1);
	}
	
	public static Color lerp(Color color1, Color color2, float percent) {
		
		int r = (int) (color1.getRed() + percent * (color2.getRed() - color1.getRed()));
		int g = (int) (color1.getGreen() + percent * (color2.getGreen() - color1.getGreen()));
		int b = (int) (color1.getBlue() + percent * (color2.getBlue() - color1.getBlue()));
		
		return new Color(r,g,b);
	}
	
	public static Vector3f lerp(Vector3f v1, Vector3f v2, float percent) {
		
		float x = lerp(v1.x, v2.x, percent);
		float y = lerp(v1.y, v2.y, percent);
		float z = lerp(v1.z, v2.z, percent);
		
		return new Vector3f(x,y,z);
	}
	
	public static float dot(Vector3f v1, Vector3f v2) {
		return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
	}
	
	public static Vector3f cross(Vector3f v1, Vector3f v2) {
		return new Vector3f(v1.y * v2.z - v1.z * v2.y, v1.z * v2.x - v1.x * v2.z, v1.x * v2.y - v1.y*v2.x);
	}
	
	public static Vector3f sub(Vector3f v1, Vector3f v2) {
		return new Vector3f(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
	}
	
	public static Vector3f add(Vector3f v1, Vector3f v2) {
		return new Vector3f(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
	}
	
	public static Vector3f mult(Vector3f v1, Vector3f v2) {
		return new Vector3f(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z);
	}
	
	public static Vector3f div(Vector3f v1, Vector3f v2) {
		return new Vector3f(v1.x / v2.x, v1.y / v2.y, v1.z / v2.z);
	}
	
	public static Vector3f mult(Vector3f v1, float s) {
		return new Vector3f(v1.x * s, v1.y * s, v1.z * s);
	}
	
	public static Vector3f div(Vector3f v1, float s) {
		return new Vector3f(v1.x / s, v1.y / s, v1.z / s);
	}
	
	public static float distance(Vector3f v1, Vector3f v2) {
		return (float) Math.sqrt(Math.pow(v2.x - v1.x,2) + Math.pow(v2.y - v1.y,2) + Math.pow(v2.z - v1.z,2));
	}
	
	public static float magnitude(Vector3f v) {
		return (float) Math.sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
	}
	
	public static Color add(Color a, Color b) {
		int R = a.getRed() + b.getRed();
		int G = a.getGreen() + b.getGreen();
		int B = a.getBlue() + b.getBlue();
		if(R > 255) R = 255;
		if(G > 255) G = 255;
		if(B > 255) B = 255;
		return new Color(R, G, B);
	}
	
	public static Color average(Color a, Color b) {
		return new Color((a.getRed() + b.getRed()) / 2, (a.getGreen() + b.getGreen()) / 2, (a.getBlue() + b.getBlue()) / 2);
	}
	
	public static Color mult(Color a, float scale) {
		int R = (int) (a.getRed() * scale);
		int G = (int) (a.getGreen() * scale);
		int B = (int) (a.getBlue() * scale);
		if(R > 255) R = 255;
		if(G > 255) G = 255;
		if(B > 255) B = 255;
		if(R < 0) R = 0;
		if(G < 0) G = 0;
		if(B < 0) B = 0;
		return new Color(R, G, B);
	}
	
	public static Color spectrumMult(Color light, Color spectrum) {
		int R = (int) (light.getRed() * (spectrum.getRed() / 255.0f));
		int G = (int) (light.getGreen() * (spectrum.getGreen() / 255.0f));
		int B = (int) (light.getBlue() * (spectrum.getBlue() / 255.0f));
		if(R > 255) R = 255;
		if(G > 255) G = 255;
		if(B > 255) B = 255;
		if(R < 0) R = 0;
		if(G < 0) G = 0;
		if(B < 0) B = 0;
		return new Color(R, G, B);
	}
	
	public static float intensity(Color c) {
		return 0.2126f * c.getRed() + 0.7152f * c.getGreen() + 0.0722f * c.getBlue();
	}
	
	public static Vector3f rotatePoint(Vector3f point, Vector3f rotation) {
		
		Vector3f newPoint;
		
		//rotate around the X axis
		newPoint = new Vector3f(
				point.x,
				(float)(point.y * Math.cos(rotation.x) - point.z * Math.sin(rotation.x)),
				(float)(point.y * Math.sin(rotation.x) + point.z * Math.cos(rotation.x)));
		
		//rotate around the Y axis
		newPoint = new Vector3f(
				(float)(newPoint.x * Math.cos(rotation.y) + newPoint.z * Math.sin(rotation.y)), 
				newPoint.y,
				(float)(-newPoint.x * Math.sin(rotation.y) + newPoint.z * Math.cos(rotation.y)));
		
		//rotate around the Z axis
		newPoint = new Vector3f(
				(float)(newPoint.x * Math.cos(rotation.z) - newPoint.y * Math.sin(rotation.z)),
				(float)(newPoint.x * Math.sin(rotation.z) + newPoint.y * Math.cos(rotation.z)),
				newPoint.z);
		
		
		return newPoint;
	}
	
	public static Vector3f rotateAround(Vector3f point, Vector3f axis, Vector3f rotation) {
		axis.normalize();
		//rotate around the Y axis
		Vector3f tempPoint = Util.sub(point, axis);
		Vector3f newPoint = new Vector3f(
				(float)(tempPoint.x * Math.cos(rotation.y) + tempPoint.z * Math.sin(rotation.y)), 
				tempPoint.y,
				(float)(-tempPoint.x * Math.sin(rotation.y) + tempPoint.z * Math.cos(rotation.y)));
		
		//next rotate around the X axis
		newPoint = new Vector3f(
				newPoint.x,
				(float)(newPoint.y * Math.cos(rotation.x) - newPoint.z * Math.sin(rotation.x)),
				(float)(newPoint.y * Math.sin(rotation.x) + newPoint.z * Math.cos(rotation.x)));
		
		//next rotate around the Z axis
		newPoint = new Vector3f(
				(float)(newPoint.x * Math.cos(rotation.z) - newPoint.y * Math.sin(rotation.z)),
				(float)(newPoint.x * Math.sin(rotation.z) + newPoint.y * Math.cos(rotation.z)),
				newPoint.z);
		
		
		return Util.add(newPoint,axis);
	}
	
	public static Vector3f rotateAroundAxis(Vector3f point, Vector3f axis, float angle) {
		axis.normalize();
		//Vector3f finalPoint = new Vector3f(0);
		
		Vector3f X = Util.mult(axis, Util.dot(axis, point));
		Vector3f Y = Util.cross(Util.mult(Util.cross(axis, point),(float)Math.cos(angle)), axis);
		Vector3f Z = Util.mult(Util.cross(axis, point),(float)Math.sin(angle));
		Vector3f finalPoint = Util.add( Util.add(X,Y),Z);
		
		return finalPoint;
	}
	
	public static Vector3f refractedRay(Vector3f incident, Vector3f normal, float n1, float n2) {
		
		float ndi = Util.dot(normal, incident);
		float mu = n1 / n2;
		
		float A = 1 - ndi * ndi;
		float B = (float) Math.sqrt(1 - (mu * mu) * A);
		Vector3f C = Util.mult(normal, B);
		
		Vector3f D = Util.mult(normal,ndi);
		Vector3f E = Util.sub(incident, D);
		Vector3f F = Util.mult(E, mu);
		
		Vector3f T = Util.add(C, F);
		
		return T;
	}
	
}

