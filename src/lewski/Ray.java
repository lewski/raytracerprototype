package lewski;

import org.joml.Vector3f;

public class Ray {

	private Vector3f origin;
	private Vector3f direction;
	
	public Ray(Vector3f o, Vector3f d) {
		origin = o;
		direction = d.normalize();
	}
	
	public Vector3f getOrigin() { return origin; }
	public Vector3f getDirection() { return direction; }
	
	public void setOrigin(Vector3f o) {
		origin = o;
	}
	
	public void setDirection(Vector3f d) {
		direction = d.normalize();
	}
	
}
