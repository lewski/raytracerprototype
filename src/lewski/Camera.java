package lewski;

import org.joml.Matrix4f;
import org.joml.Vector3f;

public class Camera {

	private Vector3f position; //camera position in 3d space
	private Vector3f direction; //position in 3d space the camera is pointed at
	private Vector3f up; //directional vector for which was is 'up' relative to the camera view.
	
	private float fov; //the angle between the viewing frustums horizontally
	
	public Camera () {
		position = new Vector3f(0,0,-3);
		direction = new Vector3f(0,0,1).normalize();
		up = new Vector3f(0,1,0).normalize();
		
		fov = 60;
	}
	public Vector3f getPosition() { return position; }
	public Vector3f getDirection() { return direction; }
	public Vector3f getUp() { return up; }
	public float getFOV() { return fov; }
	
	public void setPosition(Vector3f p) {
		position = p;
	}
	
	public void setDirection(Vector3f d) {
		direction = d.normalize();
	}
	
	public void setUp(Vector3f u) {
		up = u.normalize();
	}
	
	public void setFOV(float f) {
		fov = f;
	}
}
