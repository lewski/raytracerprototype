package lewski;

import javax.swing.JFrame;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;

public class OpenGLDisplay implements GLEventListener {

	public String title = "OpenGL Render";
	
	public int scale = 3;
	public int width = 600 * scale;
	public int height = width / 16 * 9;
	
	public JFrame frame;
	
	public OpenGLDisplay() {
	
		final GLProfile profile = GLProfile.get(GLProfile.GL2);
		GLCapabilities capabilities = new GLCapabilities(profile);
		
		final GLCanvas glcanvas = new GLCanvas(capabilities);
		glcanvas.addGLEventListener(this);
		glcanvas.setSize(width,height);
		
		frame = new JFrame();
		frame.getContentPane().add(glcanvas);
		
		frame.setTitle(title);
		frame.setSize(frame.getContentPane().getPreferredSize());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	@Override
	public void display(GLAutoDrawable drawable) {
		final GL2 gl = drawable.getGL().getGL2();
		
	      //edge1  
	      gl.glBegin( GL2.GL_LINES );
	      gl.glVertex3f( 0.0f,0.75f,0 );
	      gl.glVertex3f( -0.75f,0f,0 );
	      gl.glEnd();
	      
	      //edge2
	      gl.glBegin( GL2.GL_LINES );
	      gl.glVertex3f( -0.75f,0f,0 );
	      gl.glVertex3f( 0f,-0.75f, 0 );
	      gl.glEnd();
	      
	      //edge3
	      gl.glBegin( GL2.GL_LINES );
	      gl.glVertex3f( 0f,-0.75f, 0 );
	      gl.glVertex3f( 0.75f,0f, 0 );
	      gl.glEnd();
	      
	      gl.glFlush();

	}

	@Override
	public void dispose(GLAutoDrawable arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init(GLAutoDrawable arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reshape(GLAutoDrawable arg0, int arg1, int arg2, int arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}
}