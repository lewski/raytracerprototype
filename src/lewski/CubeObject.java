package lewski;

import org.joml.Matrix3f;
import org.joml.Vector2f;
import org.joml.Vector3f;

public class CubeObject extends MeshObject {

	public CubeObject(Vector3f pos) {

		//Cube vertices
		vertexPosition.add(new Vector3f(-0.5f, 0.5f, -0.5f).add(pos));
		vertexPosition.add(new Vector3f(-0.5f, -0.5f, -0.5f).add(pos));
		vertexPosition.add(new Vector3f(0.5f, -0.5f, -0.5f).add(pos));
		vertexPosition.add(new Vector3f(0.5f, 0.5f, -0.5f).add(pos));
		vertexPosition.add(new Vector3f(-0.5f, 0.5f, 0.5f).add(pos));
		vertexPosition.add(new Vector3f(-0.5f, -0.5f, 0.5f).add(pos));
		vertexPosition.add(new Vector3f(0.5f, -0.5f, 0.5f).add(pos));
		vertexPosition.add(new Vector3f(0.5f, 0.5f, 0.5f).add(pos));
		
		vertexUV.add(new Vector2f(0.0f, 0.0f));
		vertexUV.add(new Vector2f(1.0f, 0.0f));
		vertexUV.add(new Vector2f(0.0f, 1.0f));
		vertexUV.add(new Vector2f(1.0f, 1.0f));
		
		vertexNormal.add(new Vector3f(0,0,-1)); //front
		vertexNormal.add(new Vector3f(0,0,1)); //back
		vertexNormal.add(new Vector3f(0,-1,0)); //bottom
		vertexNormal.add(new Vector3f(0,1,0)); //top
		vertexNormal.add(new Vector3f(-1,0,0)); //left
		vertexNormal.add(new Vector3f(1,0,0)); //right
		
		//Front face
		faces.add(new Matrix3f(new Vector3f(0,2,0),new Vector3f(3,3,0),new Vector3f(1,0,0)));
		faces.add(new Matrix3f(new Vector3f(3,3,0),new Vector3f(2,2,0),new Vector3f(1,0,0)));
		
		//Back face
		faces.add(new Matrix3f(new Vector3f(7,2,1),new Vector3f(4,3,1),new Vector3f(6,0,1)));
		faces.add(new Matrix3f(new Vector3f(6,0,1),new Vector3f(4,3,1),new Vector3f(5,1,1)));

		//Left face
		faces.add(new Matrix3f(new Vector3f(4,2,4),new Vector3f(0,1,4),new Vector3f(5,0,4)));
		faces.add(new Matrix3f(new Vector3f(0,3,4),new Vector3f(1,1,4),new Vector3f(5,2,4)));

		//Right face
		faces.add(new Matrix3f(new Vector3f(3,2,5),new Vector3f(7,3,5),new Vector3f(2,0,5)));
		faces.add(new Matrix3f(new Vector3f(2,0,5),new Vector3f(7,3,5),new Vector3f(6,1,5)));

		//Top face
		faces.add(new Matrix3f(new Vector3f(4,2,3),new Vector3f(7,3,3),new Vector3f(0,0,3)));
		faces.add(new Matrix3f(new Vector3f(0,0,3),new Vector3f(7,3,3),new Vector3f(3,1,3)));

		
		/*
		//Top face
		vertexIndex.add(4);
		vertexIndex.add(7);
		vertexIndex.add(0);
		vertexIndex.add(0);
		vertexIndex.add(7);
		vertexIndex.add(3);

		//Bottom face
		vertexIndex.add(1);
		vertexIndex.add(6);
		vertexIndex.add(5);
		vertexIndex.add(2);
		vertexIndex.add(6);
		vertexIndex.add(1);
		*/
		
		origin = calculateCenter();
	}
	
}

