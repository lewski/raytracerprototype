package lewski;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import org.joml.Matrix3f;
import org.joml.Vector2f;
import org.joml.Vector3f;

public class ModelLoader {

	private String sphere1 = "res/floor1.obj";
	
	static int maxObjs = 10;
	static int objCount = 0;
	
	public ModelLoader() {
		loadModel(sphere1);
	}
	
	public static MeshObject loadModel(String filePath) {
		
		MeshObject newObject = new MeshObject();
		
		BufferedReader reader;
		
		try {
			reader = new BufferedReader(new FileReader(filePath));
			String line = reader.readLine();
			System.out.println("Loading " + filePath);
			while(line != null) {
				String[] splitLine = line.split(" ");
				//Object
				if(splitLine[0].contentEquals("o")) {
					if(objCount > maxObjs) {
						return newObject;
					}
					System.out.println("\tLoading " + splitLine[1] + " object.");
					objCount ++;
				}
				//Vertex Position
				if(splitLine[0].contentEquals("v")) {
					newObject.vertexPosition.add(new Vector3f(Float.parseFloat(splitLine[1]), Float.parseFloat(splitLine[2]), Float.parseFloat(splitLine[3])));
				}
				//Vertex UV
				if(splitLine[0].contentEquals("vt")) {
					newObject.vertexUV.add(new Vector2f(Float.parseFloat(splitLine[1]), Float.parseFloat(splitLine[2])));
				}
				//Vertex Normal
				if(splitLine[0].contentEquals("vn")) {
					newObject.vertexNormal.add(new Vector3f(Float.parseFloat(splitLine[1]), Float.parseFloat(splitLine[2]), Float.parseFloat(splitLine[3])));
				}
				//Face data
				if(splitLine[0].contentEquals("f")) {
					String[] faceData = splitLine[1].split("/");
					Vector3f v1 = new Vector3f(Integer.parseInt(faceData[0])-1,Integer.parseInt(faceData[2])-1,Integer.parseInt(faceData[2])-1);
					faceData = splitLine[2].split("/");
					Vector3f v2 = new Vector3f(Integer.parseInt(faceData[0])-1,Integer.parseInt(faceData[2])-1,Integer.parseInt(faceData[2])-1);
					faceData = splitLine[3].split("/");
					Vector3f v3 = new Vector3f(Integer.parseInt(faceData[0])-1,Integer.parseInt(faceData[2])-1,Integer.parseInt(faceData[2])-1);
					newObject.faces.add(new Matrix3f(v1,v2,v3));
				}
				
				//System.out.println(line);
				
				line = reader.readLine();
			}
			reader.close();
		}catch (IOException e) {
			e.printStackTrace();
		}
		
		return newObject;
	}
	
}
