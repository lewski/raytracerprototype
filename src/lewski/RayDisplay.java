package lewski;

import java.awt.Color;
import java.awt.image.Kernel;
import java.util.Random;

import org.joml.Vector3f;


public class RayDisplay {


	public String title = "Ray Trace Render";
	
	public final int width;
	public final int height;

	public static int[] pixels;

	public Random rand = new Random();
	
	public static Scene currentScene;
	
	public RayDisplay(Scene scene, int w, int h) {
		width = w;
		height = h;
		
		pixels = new int[width * height];

		currentScene = scene;
	}
		
	public void clearDisplay() {
		for(int i = 0; i < pixels.length; i++){
			pixels[i] = 0;
		}
	}
	
	public void randomizeDisplay(int max) {
		for(int i = 0; i < pixels.length; i++){
			pixels[i] = rand.nextInt(max);
		}
	}
	
	public void setDisplay(int value) {
		for(int i = 0; i < pixels.length; i++){
			pixels[i] = value;
		}
	}
	
	public void setDisplay(int[] values) {
		for(int i = 0; i < pixels.length; i++){
			pixels[i] = values[i];
		}
	}
	
	public void gradiantDisplay() {
		for(int i = 0; i < pixels.length; i++){
			pixels[i] = i * (0xFF00FF / pixels.length);
		}
	}
	
	public void rayTraceDisplay() {
		
		float min = 99999;
		float max = -99999;

		float range = 0;
		float mid = 0;
		
		Vector3f rayOrigin = currentScene.mainCamera.getPosition();
		Vector3f rayCenterDir = currentScene.mainCamera.getDirection();
		Vector3f cameraUp = currentScene.mainCamera.getUp();

		float fov = currentScene.mainCamera.getFOV();
		float scale =  (float) Math.tan(fov / 2.0f * Math.PI / 180.0f);
		float imageAspectRatio = (float)width / (float)height;
		
		int shift = 1;
		
		rand = new Random(12345);
		if(currentScene.moving) {
			//shift = 153;
		}
		
		

		
		for(int i = 0; i < pixels.length; i += shift) {
						
			int x = i % width;
			int y = i / width;
			
			float offsetX = 0.5f;
			float offsetY = 0.5f;
			//float offsetX = rand.nextFloat() * 2.0f - 1.0f;
			//float offsetY = rand.nextFloat() * 2.0f - 1.0f;
			
			float Px = (float) ((2 * (x + offsetX) / (float)width - 1) * scale * imageAspectRatio);
			float Py = (float) ((1 - 2 * ((y + offsetY) / (float)height)) * scale );
			
			Vector3f rightVec = Util.cross(rayCenterDir, cameraUp).normalize();
			Vector3f horizontalShift = Util.mult(rightVec,Px);
			Vector3f verticalShift = Util.mult(Util.cross(rightVec, rayCenterDir).normalize(), Py);
			
			Vector3f rayDir = Util.add(Util.add(rayCenterDir, horizontalShift), verticalShift).normalize();//Util.sub(, rayOrigin);//.normalize();

			Ray scanRay = new Ray(rayOrigin, rayDir);

			pixels[i] = RayLightCalculator.getLight(scanRay, 1.0f, Color.black, 0).getRGB();
			
			
			if(Px < min) min = Px;
			if(Px > max) max = Px;
			range = max - min;
			mid =  scale * imageAspectRatio;

		}
		//System.out.println(imageAspectRatio);
	}
	
	
}
