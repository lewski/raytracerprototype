package lewski;

import org.joml.Vector3f;

public class Light {

	private Vector3f position;
	private float intensity;
	
	public Light( Vector3f p, float i) {
		position = p;
		intensity = i;
	}
	
	public Vector3f getPosition() { return position; }
	public float getIntensity() { return intensity; }
	
	public void setPosition(Vector3f p) {
		position = p;
	}
	
	public void setIntensity(float i) {
		intensity = i;
	}
}
